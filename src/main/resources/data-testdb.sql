/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Version : 50712
 Source Host           : 172.16.232.131
 Source Database       : testdb

 Target Server Version : 50712
 File Encoding         : utf-8

 Date: 10/10/2016 16:05:32 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `t_info_0`
-- ----------------------------
DROP TABLE IF EXISTS `t_info_0`;
CREATE TABLE `t_info_0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `age` tinyint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_info_0`
-- ----------------------------
BEGIN;
INSERT INTO `t_info_0` VALUES ('1', 'test2', '28', '2'), ('2', 'test4', '30', '4');
COMMIT;

-- ----------------------------
--  Table structure for `t_info_1`
-- ----------------------------
DROP TABLE IF EXISTS `t_info_1`;
CREATE TABLE `t_info_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `age` tinyint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_info_1`
-- ----------------------------
BEGIN;
INSERT INTO `t_info_1` VALUES ('1', 'test1', '50', '1'), ('2', 'test3', '20', '3');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
