package com.phoenix;

import com.google.common.collect.Maps;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("")
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	private UserMapper userMapper;

	@RequestMapping(value = "/u/{id}",method = RequestMethod.GET)
	public Map<String,Object> getUser(@PathVariable int id){
		return userMapper.getUser(id);
	}

	@RequestMapping(value = "/u/add",method = RequestMethod.POST)
	public String save(HttpServletRequest request){
		Map<String,Object> map = Maps.newHashMap();
		map.put("name",request.getParameter("name"));
		map.put("age",request.getParameter("age"));
		map.put("user_id",Integer.parseInt(request.getParameter("user_id")));
		int result = userMapper.save(map);
		if(result == 0){
			return "failed";
		}
		return "success";
	}
}
