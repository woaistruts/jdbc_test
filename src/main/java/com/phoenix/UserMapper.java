package com.phoenix;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

/**
 * Created by lilongfei on 16/10/9.
 */
@Mapper
public interface UserMapper {

    @Select("select * from t_info where user_id = #{id} ")
    Map<String,Object> getUser(int id);

    @Insert(" INSERT INTO `t_info` (name,age,user_id) VALUES (#{name}, #{age}, #{user_id}) ")
    int save(Map<String,Object> map);
}
